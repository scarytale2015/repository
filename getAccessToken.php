<?php
$subdomain = 'scarytale2015';
$link = 'https://' . $subdomain . '.kommo.com/oauth2/access_token';

$data = [
    'client_id' => '5f10c4fb-4099-4048-9dc5-42b5d5866340',
    'client_secret' => 'sRpH9ClE91bIm2vjFfPhYUkHxwiyQCrEjcvpHmC8G4anN7Pm6Yxn1kcPAbRsjzOY',
    'grant_type' => 'authorization_code',
    'code' => 'def502009c59588e090b2ec8402060abd74c5671ab658d49ead0234d9049fe8fec3166764525f3ffefee07cd077abb3570d047c6a4e04e6818d1e805260563ae9273e7ba70e3a31daa0b1049c31bd2cb3c987b26af58945761ac860b3a07ae86aa9c502ab7fe6cec4a9bf8ee95acd540e475adc7b7ed79a743b510502cd43d3430944c4e7a54e3946734ff14c0b950f156f289c6ea64b4d40cdee51baa40fb08a0044ebad51058a82c98e80cbb05b2f13bc03f88ec0025b8c8b5a9b3ce90166ed568e406fe4fa5ea6515980138be0a4de5b9ca568d975f7a82e08236354ea20d3b3fe3a6d251956ac55b683a9b403a3cdc65ce2f0f828c81255afc51c67ec34355584208fd54cd7b3724f410317398171ecaaf0001afe9206047f3c44d02378202c87490cd32dfbe5b1f66283507453ef985dbe51d67733dadd43c39206ea80485ee8e2ea5aeb723415c172777dec9c467a7694a4cbdbd47895b23e852ccf096df641754d6ecdff5c81d887a6c76ee02a843c706fa1f638c5349a7ae70da8141d95a88f0d36dcf42599026a86d941e3f6288c0aaf50d9c83bf3bae3127f39f04af23f39af1345095c8561966d44378d97effd259a7c8d2a1728b2a54c739fae5dfa37011294dab90da6096baf3cb4ee112f5407f5ec97a9a984e4d082a65aed8d58ad790a307f57ba7864c41e388d8',
    'redirect_uri' => 'https://scarytale2015.kommo.com',
];

$curl = curl_init();

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_USERAGENT, 'amo-oAuth-client/1.0');
curl_setopt($curl, CURLOPT_URL, $link);
curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
$out = curl_exec($curl);
$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

$code = (int)$code;
$errors = [
    400 => 'Bad request',
    401 => 'Unauthorized',
    403 => 'Forbidden',
    404 => 'Not found',
    500 => 'Internal server error',
    502 => 'Bad gateway',
    503 => 'Service unavailable',
];

try {
    if ($code < 200 || $code > 204) {
        throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
    }
} catch (\Exception $e) {
    die('Error: ' . $e->getMessage() . PHP_EOL . 'Error code: ' . $e->getCode());
}

$response = json_decode($out, true);

$access_token = $response['access_token']; //Access token

file_put_contents('secrettoken.txt', $access_token);