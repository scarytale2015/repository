<?php

$subdomain = 'scarytale2015';
header("Access-Control-Allow-Origin: https://$subdomain.kommo.com");
$lead_id = filter_input(INPUT_GET, 'lead_id', FILTER_VALIDATE_INT);
$link = 'https://' . $subdomain . '.kommo.com/api/v4/leads/' . $lead_id . '?with=catalog_elements'; //Creation of URL for request
$access_token = file_get_contents('secrettoken.txt');

$headers = [
    'Authorization: Bearer ' . $access_token
];

function getUrl($link)
{
    global $headers;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
    $out = curl_exec($curl);
    curl_close($curl);
    return json_decode($out, true);
}

function getProductName($catalog_id, $product_id)
{
    global $subdomain;
    $out = getUrl('https://' . $subdomain . '.kommo.com/api/v4/catalogs/' . $catalog_id . '/elements/' . $product_id);
    return $out['name'];
}

function getProduct($product)
{
    $catalog_id = $product['metadata']['catalog_id'];
    $product_id = $product['id'];
    $name = getProductName($catalog_id, $product_id);
    $quantity = $product['metadata']['quantity'];

    return [
        'name' => $name,
        'quantity' => $quantity,
    ];
}

$out = getUrl($link);

try {
    $products = array_map('getProduct', $out['_embedded']['catalog_elements']);

    echo json_encode($products, JSON_UNESCAPED_UNICODE);
} catch (\Exception $e) {
    die('Error: ' . $e->getMessage() . PHP_EOL);
}